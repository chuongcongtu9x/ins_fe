import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-typescript-test',
  templateUrl: './typescript-test.component.html',
})
export class TypescriptTestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    // this.defaultFnc(3);

    // const sum = this.sumAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    // console.log('sum', sum);

    // this.spreadOperator();

    // this.objSpreadOperator();
  }

  // default fnc params
  defaultFnc(a: any, b = 25) {
    console.log('a', a);
    console.log('b', b);
  }

  // rest params
  sumAll(mess: any, ...arr: any) {
    return arr.reduce((prev: number, next: number) => prev + next);
  }

  // arr spread operator
  spreadOperator() {
    const toppings = ['bacon', 'chilli'];
    const newToppings = ['pepperoni'];
    const allToppings = [...newToppings, ...toppings];
    console.log(allToppings);
  }

  // obj spread operator
  objSpreadOperator() {
    const pizza = {
      name: 'Pepperoni',
    };
    const toppings = ['pepperoni'];
    const spreadOrder = { ...pizza, name:toppings };
    console.log(spreadOrder);
  }

  // destructuring arr obj
  desArrObj() {
    const person = {
      name: "John",
      age: 30,
      gender: "male",
    };

    // Trích xuất giá trị từ đối tượng vào các biến riêng lẻ
    const { name, age, gender } = person;
  }
}
